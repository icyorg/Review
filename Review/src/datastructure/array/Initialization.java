package datastructure.array;

import java.util.Arrays;

public class Initialization {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a;
		int[] b = new int[5];
		System.out.println(Arrays.toString(b));
		int[] c = new int[]{1,2,3};
		System.out.println(c.length);
		System.out.println(Arrays.toString(c));
		int[] d = {};
	}

}
