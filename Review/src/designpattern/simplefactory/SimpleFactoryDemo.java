package designpattern.simplefactory;

public class SimpleFactoryDemo {

	public static void main(String[] args) {
		ShapeFactory shapeFactory = new ShapeFactory();
		
		//get an object of Circle and call its draw method.
		Shape shape1 = shapeFactory.getShape("Circle");
		//call draw method of Circle
		shape1.draw();
		
		Shape shape2 = shapeFactory.getShape("Rectangle");
		shape2.draw();
		
		Shape shape3 = shapeFactory.getShape("Square");
		shape3.draw();
	}

}
