package designpattern.simplefactory;

public class ShapeFactory {
	
	private static final String CIRCLE = "circle";
	private static final String RECTANGLE = "rectangle";
	private static final String SQUARE = "square";
	
	//use getShape method to get object of type shape
	public Shape getShape(String shapeType) {
		if(null == shapeType) {
			return null;
		}
		if(CIRCLE.equalsIgnoreCase(shapeType)) {
			return new Circle();
		} else if(RECTANGLE.equalsIgnoreCase(shapeType)) {
			return new Rectangle();
		} else if(SQUARE.equalsIgnoreCase(shapeType)) {
			return new Square();
		}
		
		return null;
	}
}
