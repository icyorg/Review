package designpattern.simplefactory;

public interface Shape {
	void draw();
}
