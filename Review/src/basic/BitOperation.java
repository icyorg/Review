package basic;

public class BitOperation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 60;
		int b = 13;
		int c = 0;
		
		System.out.println("a = " + a + " = " + Integer.toBinaryString(a));
		System.out.println("b = " + b + " = " + Integer.toBinaryString(b));
		System.out.println("c = " + c + " = " + Integer.toBinaryString(c));
		
		System.out.println("a & b = " + (a&b) + " = " + Integer.toBinaryString(a&b));
		System.out.println("a | b = " + (a|b) + " = " + Integer.toBinaryString(a|b));
		System.out.println("a ^ b = " + (a^b) + " = " + Integer.toBinaryString(a^b));
		System.out.println("a<<2 = " + (a<<2) + " = " + Integer.toBinaryString(a<<2));
		System.out.println("a>>2 = " + (a>>2) + " = " + Integer.toBinaryString(a>>2));
		System.out.println("a>>>2 = " + (a>>>2) + " = " + Integer.toBinaryString(a>>>2));
		
	}

}
